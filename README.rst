========
numtools
========

numeric-gmbH toolbox

Packages
--------

* ``csyslib``: basic coordinates transformations
* ``intzip``: convert sequence of integers into human-friendly string (and vice-versa)
* ``serializer``: mix in handling serialization to msgpack and json.
* ``vgextended``: extension of ``vg`` built on top of ``vg`` and ``numpy``

