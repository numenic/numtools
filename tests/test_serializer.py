#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `numtools.serializer` package."""

from collections import UserDict

import numpy as np
import pandas as pd
import pytest
from pandas.testing import assert_frame_equal, assert_series_equal

from numtools import serializer
from numtools.serializer import IS_MSGPACK, Serializer, set_loglevel

try:
    import networkx as nx

    IS_NETWORKX = True
except ImportError:
    IS_NETWORKX = False


class MyData(UserDict, Serializer):
    def __init__(self):
        super().__init__()
        self.a = 5

    def __repr__(self):
        return "MyData"


class MyStruc(Serializer):
    def __init__(self):
        self.data = {"a": True, "mydata": MyData(), "processed": set()}

    def __repr__(self):
        return "MyStruc"


@pytest.mark.skipif(IS_MSGPACK is False, reason="no msgpack")
def test_dic2_msgpack():
    set_loglevel("info")

    mystruc = MyStruc()
    mystruc.data["mydata"][5] = "hello"
    mystruc.data["mydata"][6] = "world!"
    mystruc.data["mydata"]["6"] = "... and the rest of the universe"
    mystruc.data["mydata"]["s"] = set((1, 2, 3))

    md1 = MyStruc()
    md1.from_msgpack(mystruc.to_msgpack())
    assert md1.data["mydata"][5] == "hello"
    assert md1.data["mydata"][6] == "world!"
    assert md1.data["mydata"]["6"] == "... and the rest of the universe"
    assert md1.data["mydata"]["s"] == set((1, 2, 3))


def test_dic2_json():
    set_loglevel("info")

    mystruc = MyStruc()
    mystruc.data["mydata"][5] = "hello"
    mystruc.data["mydata"][6] = "world!"
    mystruc.data["mydata"]["6"] = "... and the rest of the universe"
    mystruc.data["mydata"]["s"] = set((1, 2, 3))

    md1 = MyStruc()
    md1.from_json(mystruc.to_json())
    assert md1.data["mydata"][5] == "hello"
    # key#6 has been overwritten silently by JSON :-/
    assert md1.data["mydata"][6] == "... and the rest of the universe"
    assert md1.data["mydata"]["s"] == set((1, 2, 3))

    # do not convert integer keys:

    serializer.FIX_JSON_INTAGER_KEYS = False
    md1 = MyStruc()
    md1.from_json(mystruc.to_json())
    assert md1.data["mydata"]["5"] == "hello"
    # key#6 has been overwritten silently by JSON :-/
    assert md1.data["mydata"]["6"] == "... and the rest of the universe"
    assert md1.data["mydata"]["s"] == set((1, 2, 3))


@pytest.mark.skipif(IS_MSGPACK is False, reason="no msgpack")
def test_numpy_msgpack():
    import numpy as np

    class Test(UserDict, Serializer):
        pass

    t = Test()
    t.array = np.array([[1, 2, 3], [4, 5, 6]])
    t2 = Test()
    t2.from_msgpack(t.to_msgpack())
    assert np.all(t2.array == t.array)


def test_numpy_json():

    class Test(UserDict, Serializer):
        pass

    t = Test()
    t.array = np.array([[1, 2, 3], [4, 5, 6]])
    t2 = Test()
    t2.from_json(t.to_json())
    assert np.all(t2.array == t.array)


@pytest.mark.skipif(IS_MSGPACK is False, reason="no msgpack")
@pytest.mark.skipif(not IS_NETWORKX, reason="networkx is not installed")
def test_nx_sgpack():

    class Test(UserDict, Serializer):
        pass

    G1 = nx.Graph()
    G1.add_edges_from([(1, 2, {"eid": 1}), (1, 3, {"eid": 2}), (3, 2, {"eid": 3})])
    t = Test()
    t.graph = G1
    t2 = Test()
    t2.from_msgpack(t.to_msgpack())
    assert t.graph.edges() == t2.graph.edges()
    assert t.graph.nodes() == t2.graph.nodes()


@pytest.mark.skipif(not IS_NETWORKX, reason="networkx is not installed")
def test_nx_son():

    class Test(UserDict, Serializer):
        pass

    G1 = nx.Graph()
    G1.add_edges_from([(1, 2, {"eid": 1}), (1, 3, {"eid": 2}), (3, 2, {"eid": 3})])
    t = Test()
    t.graph = G1
    t2 = Test()
    t2.from_json(t.to_json())
    assert t.graph.edges() == t2.graph.edges()
    assert t.graph.nodes() == t2.graph.nodes()


def test_pandas_dataframe():

    class Test(UserDict, Serializer):
        pass

    df = pd.DataFrame(
        {"a": [1, 2, 3], "b": ["a", "b", "c"], "c": [0, 0, 1], "d": [1.2, 5.6, 3.7]}
    )
    ser0 = pd.Series([1, 2, "4"])
    ser1 = pd.Series([1, 2, 4.1], index=["a", "b", "c"], name="toto")
    t = Test()
    t.df = df
    t.df_multi_index = df.set_index(["a", "c"])
    t.ser0 = ser0
    t.ser1 = ser1
    t2 = Test()
    serialized = t.to_json()
    t2.from_json(serialized)

    assert_frame_equal(t.df, t2.df)
    assert_frame_equal(t.df_multi_index, t2.df_multi_index)
    assert_series_equal(t.ser0, t2.ser0)
    assert_series_equal(t.ser1, t2.ser1)
    assert t.ser0.name is t2.ser0.name is None
    assert t.ser1.name == t2.ser1.name == "toto"
    # -------------------------------------------------------------------------
    # a more complex object
    t = Test()
    t.objs = {"df0": df, "df1": df.set_index(["a", "c"]), "s0": ser0, "s1": ser1}
    serialized = t.to_json()
    t2 = Test()
    t2.from_json(serialized)

    assert_frame_equal(t.objs["df0"], t2.objs["df0"])
    assert_frame_equal(t.objs["df1"], t2.objs["df1"])
    assert_series_equal(t.objs["s0"], t2.objs["s0"])
    assert_series_equal(t.objs["s1"], t2.objs["s1"])


def test_pandas_dataframe2():
    """test serialization with integers as columns haders"""

    class Test(UserDict, Serializer):
        pass

    df = pd.DataFrame({"1": [1, 2, 3], 2: ["a", "b", "c"]})
    t = Test()
    t.df = df
    t2 = Test()
    serialized = t.to_json()
    t2.from_json(serialized)
    assert_frame_equal(t.df, t2.df)
