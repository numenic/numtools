# -*- coding: utf-8 -*-

"""Top-level package for numtools."""

__author__ = """Nicolas Cordier"""
__email__ = "nicolas.cordier@numeric-gmbh.ch"
__version__ = "3.1.0"
